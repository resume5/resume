FROM node:12.16.2-alpine as angular
WORKDIR /app
COPY . /app
RUN npm install --silent
RUN touch src/environments/version.json && \
  echo $nowDate && \
  echo '{ "version": "' ${nowDate} '" }' > ./src/environments/version.json
RUN npm run build

FROM nginx:alpine as deploy
COPY ./config/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=angular ./app/dist/resume /usr/share/nginx/html