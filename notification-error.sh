#!/bin/bash

branch=$(git name-rev HEAD)

if [ "$branch" == "HEAD remotes/origin/master" ]; then
        ENVIRONMENT="produção"
fi


if [ "$branch" == "HEAD remotes/origin/develop" ]; then
        ENVIRONMENT="desenvolvimento"
fi

curl -v \
-H "Authorization: xxx" \
-H "User-Agent: Servidor Git (http://xxx, v0.1)" \
-H "Content-Type: application/json" \
-X POST \
-d "{\"content\":\"Houve erro no momento de subir a aplicação front de administrador no ambiente de ${ENVIRONMENT} no Curia 🚨\"}" \
https://discordapp.com/api/webhooks/xxx
