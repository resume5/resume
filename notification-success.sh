#!/bin/bash

branch=$(git name-rev HEAD)

if [ "$branch" == "HEAD remotes/origin/master" ]; then
        ENVIRONMENT="produção"
        SITE="xxx"
fi


if [ "$branch" == "HEAD remotes/origin/develop" ]; then
        ENVIRONMENT="desenvolvimento"
        SITE="xxx"
fi


curl -v \
-H "Authorization: xxx" \
-H "User-Agent: Servidor Git (http://git.coopersystem.com.br, v0.1)" \
-H "Content-Type: application/json" \
-X POST \
-d "{\"content\":\"A aplicação front de administrador já subiu no ambiente de ${ENVIRONMENT} no Curia \nAcesse e veja o que eu preparei: ${SITE} 🚀\"}" \
https://discordapp.com/api/webhooks/xxx
