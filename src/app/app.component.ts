import {Component, ElementRef, ViewChild} from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {slideInAnimation} from "./slideInAnimation";
import {MatSidenav} from "@angular/material/sidenav";
import {ViewportScroller} from "@angular/common";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slideInAnimation]
})
export class AppComponent {

  constructor(private viewportScroller: ViewportScroller) {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
  @ViewChild('sidenav') sidenav: MatSidenav;

  @ViewChild('panel') public panel:ElementRef;

  @ViewChild('menu') public menu:ElementRef;

  public toMenu(element: string): void {
    this.viewportScroller.scrollToAnchor(element)
  }




}


