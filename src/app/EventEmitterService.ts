import { EventEmitter } from '@angular/core';

export class EventEmitterService {

    private static emitters: {
        [event: string]: EventEmitter<Object>
    } = {}

    static get (event: string): EventEmitter<Object> {
        if (!this.emitters[event])
            this.emitters[event] = new EventEmitter<Object>();
        return this.emitters[event];
    }

}