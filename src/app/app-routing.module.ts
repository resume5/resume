import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from "./components/pages/main/main.component";
import {ProjectsComponent} from "./components/pages/projects/projects.component";
import {ContactComponent} from "./components/pages/contact/contact.component";
import {BotComponent} from "./components/pages/bot/bot.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {BrowserModule} from "@angular/platform-browser";


const routes: Routes = [
  { path: '', component:  MainComponent, data: {animation: 'main'}},
  { path: 'projects', component:  ProjectsComponent, data: {animation: 'projects'}},
  { path: 'contact', component:  ContactComponent, data: {animation: 'contact'}},
  { path: 'bot', component:  BotComponent, data: {animation: 'bot'}},
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [BrowserModule, BrowserAnimationsModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const RoutingComponents = [MainComponent, ProjectsComponent, ContactComponent, BotComponent]

