import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

    state: string= 'closed';
    @Output() info: EventEmitter<Object> = new EventEmitter<Object>()
    @Output() bot: EventEmitter<Object> = new EventEmitter<Object>()
    @Output() projects: EventEmitter<Object> = new EventEmitter<Object>()
    @Output() contact: EventEmitter<Object> = new EventEmitter<Object>()

    showFiller = false;

  //constructor(private router: Router) { }

  ngOnInit(): void {
  }

}
