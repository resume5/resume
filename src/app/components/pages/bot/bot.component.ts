import { Component, OnInit, OnDestroy } from '@angular/core';
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";
import {EventEmitterService} from "../../../EventEmitterService";
import {AnimationAction} from "../AnimationAction";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {ChatbotModalComponent} from "../../chatbot-modal/chatbot-modal.component";
import {MatDialog} from "@angular/material/dialog";

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-bot',
  templateUrl: './bot.component.html',
  styleUrls: ['./bot.component.scss']
})
export class BotComponent {
  state: string = 'closed';
  animal: string;
  name: string;

  constructor(public dialog: MatDialog) {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ChatbotModalComponent, {
      width: '480px',
      data: {name: this.name, animal: this.animal}
    });

    }
}
