import { Component, OnInit } from '@angular/core';
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";
import {EventEmitterService} from "../../../EventEmitterService";
import {AnimationAction} from "../AnimationAction";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  state: String = 'closed';

  constructor() {
  }

  ngOnInit(): void {
  }

}
