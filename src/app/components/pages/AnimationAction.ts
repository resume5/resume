import {animate, AnimationTriggerMetadata, keyframes, state, style, transition, trigger} from "@angular/animations";

export class AnimationAction{

    static animation_settings(): AnimationTriggerMetadata[]{
        return [
            trigger('openClose', [
                state('open', style({
                    opacity: '1',
                    height: '100%',
                    width: '100%',
                })),
                state('closed', style({
                    opacity: '0',
                    width: '0px',
                    height: '100%',
                    order: -1,
                })),
                transition('open => closed', animate('1000ms', keyframes([
                    style({opacity: '1', width: '500px', transform: 'translateX(0) scaleX(1)',height: '100%', offset: 0}),
                    style({opacity: '0.0', transform: 'translateX(-100px)', offset: 0.3}),
                    style({opacity: '0', width: '0px', transform: 'translateX(-700px) scaleX(.1)', offset: 1})
                ]))),
                transition('closed => open', animate('1000ms', keyframes([
                    style({opacity: '0', width: '0px', transform: 'translateX(-700px) scaleX(.1)', offset: 0}),
                    style({opacity: '0', transform: 'translateX(-100px)', offset: 0.3}),
                    style({opacity: '1', order: -1, width: '500px',transform: 'translateX(0) scaleX(1)', offset: 1})
                ])))
            ]),
        ]
    }
}