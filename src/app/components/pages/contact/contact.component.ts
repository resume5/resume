import { Component, OnInit } from '@angular/core';
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";
import {EventEmitterService} from "../../../EventEmitterService";
import {AnimationAction} from "../AnimationAction";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  state: String = 'closed';

  constructor() {
  }

  ngOnInit(): void {
  }

}
