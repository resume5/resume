import {Component, EventEmitter, Input, OnInit, OnChanges, OnDestroy } from '@angular/core';
import {animate, keyframes, state, style, transition, trigger} from "@angular/animations";
import {EventEmitterService} from "../../../EventEmitterService";


@Component({
    selector: 'app-main-page',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnChanges, OnDestroy  {

    constructor() {

    }

    ngOnInit(): void {
        //setTimeout(() => {this.state = 'open'}, 1000);
    }

    ngOnChanges(): void{
    }

    ngOnDestroy(): void {
    }

}