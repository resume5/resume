import {Component, ElementRef, Inject, OnInit, Renderer2, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {ErrorStateMatcher} from "@angular/material/core";
import {FormControl, FormGroupDirective, NgForm, Validators} from "@angular/forms";


export interface DialogData {
    animal: string;
    name: string;
}

@Component({
    selector: 'app-chatbot-modal',
    templateUrl: './chatbot-modal.component.html',
    styleUrls: ['./chatbot-modal.component.scss']
})
export class ChatbotModalComponent implements OnInit {

    emailFormControl = new FormControl('', [
        Validators.required,
        Validators.email,
    ]);

    matcher = new MyErrorStateMatcher();
    email: string;
    submit: boolean = false; // If typed goes true
    @ViewChild('inner_chat') inner_chat: ElementRef;
    logged: boolean = false; // If click submit button goes true in order to go chatbot


    constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData,
                private renderer: Renderer2) {
    }

    ngOnInit(): void {

    }

    validate() {
        this.logged === false ? this.logged = true : this.logged = false;
    }

    send_message() {
        const div: HTMLParagraphElement = this.renderer.createElement('div');
        div.className = 'user';
        this.renderer.appendChild(this.inner_chat.nativeElement, div);

        let container = document.getElementById('chat');
        container.scrollTop = container.scrollHeight;
    }

    onKey(event: any) {
        this.email = event.target.value;
        this.submit = this.email != "";
    }
}

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
