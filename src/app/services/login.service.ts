import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

class auth {
    isValid: boolean;
    token: string;
    email: string;
    isAdmin: boolean;
}

@Injectable()
export class LoginService {
    constructor(
        private http: HttpClient,
        private router: Router
    ) {

    }

    login_service(email: string) {
        localStorage.setItem('email', email);
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'authorization': 'Bearer ' + localStorage.getItem('authorization')
            }),
            user: {
                email: email,
            }
        };

        this.http.post<auth>(environment.backend + '/sign-in', options)
            .subscribe(
                data => {
                    localStorage.setItem('email', data.email);
                    this.router.navigate(['game']).then(r => {
                        console.log(r)
                    });
                },
                error => {
                    this.router.navigate(['home']).then(r => {
                    });
                });

    }
}
