import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route} from '@angular/router';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';


class auth {
  isValid: boolean;
  token: string;
  email: string;
  isAdmin: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  private isValid = false;

  constructor(private router: Router, private http: HttpClient) {
  }

  async canActivate() {
    await this.extracted().then(() => {});
    return this.isValid;
  }

  private async extracted() {
    return new Promise(((resolve, reject) => {
      let options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'authorization': 'Bearer ' + localStorage.getItem('authorization')
        })
      };
      this.http.get<auth>(environment.backend + '/auth', options)
        .subscribe(
          data => {
            // You can access status:
            localStorage.setItem('authorization', data.token);
            localStorage.setItem('email', data.email);
            this.isValid = data.isValid;
            resolve(data.isValid);
          },
          error => {
            this.router.navigate(['login']);
            this.isValid = false;
            resolve(false);
          });
    }));

  }
}
