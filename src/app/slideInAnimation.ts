import {animate, animateChild, group, keyframes, query, style, transition, trigger} from "@angular/animations";

export const slideInAnimation =
    trigger('routeAnimations', [
        transition('* <=> *', [
            style({ position: 'relative' }),
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    opacity: '1',
                    height: '100%',
                    width: '100%',
                })
            ]),
            query(':enter', [
                style({ left: '0%'})
            ]),
            query(':leave', animateChild()),
            group([
                query(':leave', [
                    animate('400ms', keyframes([
                        style({opacity: '1', transform: 'translateX(0)', offset: 0}),
                        style({opacity: '0', transform: 'translateX(-250px)', offset: 1})
                    ]))
                ]),
                query(':enter', [
                    animate('400ms', keyframes([
                        style({opacity: '0', "flex-grow": "1", transform: 'translateX(-150px)', offset: 0}),
                        style({opacity: '1', order: -1, transform: 'translateX(0px)', offset: 1})
                    ]))
                ])
            ]),
            query(':enter', animateChild()),
        ]),
        transition('* <=> FilterPage', [
            style({ position: 'relative' }),
            query(':enter, :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    width: '100%'
                })
            ]),
            query(':enter', [
                style({ left: '-100%'})
            ]),
            query(':leave', animateChild()),
            group([
                query(':leave', [
                    animate('200ms ease-out')
                ]),
                query(':enter', [
                    animate('300ms ease-out')
                ])
            ]),
            query(':enter', animateChild()),
        ])
    ]);
