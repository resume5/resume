import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule, RoutingComponents} from './app-routing.module';
import {AppComponent} from './app.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs'
import {MenuComponent} from './components/menu/menu.component';
import {FaceHiderComponent} from './components/face-hider/face-hider.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatCardModule} from '@angular/material/card';
import {MatSidenavModule} from '@angular/material/sidenav';
import {ProjectsComponent} from './components/pages/projects/projects.component';
import {ContactComponent} from './components/pages/contact/contact.component';
import {BotComponent} from './components/pages/bot/bot.component';
import {AboutComponent} from './components/pages/main/sub_pages/about/about.component';
import {TechnologiesComponent} from './components/pages/main/sub_pages/technologies/technologies.component';
import {ExperienceComponent} from './components/pages/main/sub_pages/experience/experience.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatDialogModule} from '@angular/material/dialog';
import {ChatbotModalComponent} from './components/chatbot-modal/chatbot-modal.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {ChatbotAdminComponent} from './components/chatbot-admin/chatbot-admin.component';
import {AuthGuard} from "./services/auth-guard.service";
import {LoginService} from "./services/login.service";


@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        FaceHiderComponent,
        RoutingComponents,
        ProjectsComponent,
        ContactComponent,
        BotComponent,
        AboutComponent,
        TechnologiesComponent,
        ExperienceComponent,
        ChatbotModalComponent,
        ChatbotAdminComponent

    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MatToolbarModule,
        BrowserAnimationsModule,
        FlexLayoutModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule,
        MatTabsModule,
        MatSidenavModule,
        MatButtonToggleModule,
        MatDialogModule,
        FormsModule,
        MatInputModule,
        ReactiveFormsModule
    ],
    providers: [AuthGuard, LoginService],
    bootstrap: [AppComponent],
    entryComponents: [ChatbotModalComponent]
})
export class AppModule {
}
